﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212439.Models;

namespace WebAPI1212439.Controllers
{
    [RoutePrefix("api/2.0/users")]
    public class UsersController : ApiController
    {
        static readonly IUserRepository repository = new UserRepository();

        [Route("{UserName}")]
        public User GetUser(string UserName)
        {
            User item = repository.Get(UserName);
            if(item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        [Route("")]
        public IEnumerable<User> GetAll()
        {
            return repository.GetAll();
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult AddUser (User item)
        {
            if(repository.Add(item))
            {
                return Ok("Đã thêm");
            }
            return BadRequest("Thêm không được");
        }
        [Route("{UserName}")]
        [HttpDelete]
        public IHttpActionResult RemoveUser(string UserName)
        {
            if(repository.Remove(UserName))
            {
                return Ok("Đã xóa");
            }
            return BadRequest("Xoa khong duoc");
        }

        [Route("{UserName}")]
        [HttpPut]
        public IHttpActionResult UpdateUser(string UserName, User item)
        {
            item.UserName = UserName;
            if(repository.Update(item))
            {
                return Ok("Da sua thanh cong");
            }
            return BadRequest("Khong the sua");
        }
    
    }   
}
