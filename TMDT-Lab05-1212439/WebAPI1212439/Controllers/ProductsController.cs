﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212439.Models;

namespace WebAPI1212439.Controllers
{
    //[RoutePrefix("api/2.0/products")]
    public class ProductsController : ApiController
    {
        static readonly IProductRepository repository = new ProductRepository();

        [Route("api/2.0/products")]
        public IEnumerable<Product> GetProduct (string category)
        {
            return repository.Get(category);
        }

        [Route("api/2.0/products/{category}/{username}")]
        public IEnumerable<Product> GetProductByUser (string category, string username)
        {
            return repository.GetByUser(username, category);
        }
    }
    
}
