﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212439.Models
{
    interface IUserRepository
    {
            IEnumerable<User> GetAll();
            User Get(string UserName);
            bool Add(User item);
            bool Remove(string UserName);
            bool Update(User item);
    }
}
