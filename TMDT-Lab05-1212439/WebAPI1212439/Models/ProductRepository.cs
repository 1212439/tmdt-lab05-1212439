﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI1212439.Models;

namespace WebAPI1212439.Models
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        private int _nextId = 1;

        public ProductRepository()
        {
            Add(new Product { Name = "Intel core i3", Category = "CPU", UserName = "1212439" });
            Add(new Product { Name = "Ddr3 8GB", Category = "Ram", UserName = "1212439" });
            Add(new Product { Name = "Ddr3 4GB", Category = "Ram", UserName = "tri" });
            Add(new Product { Name = "Ddr2 2GB", Category = "Ram", UserName = "tri" });
        }

        
        public IEnumerable<Product> Get(string category)
        {
            return products.Where(p => p.Category == category);
        }

        public IEnumerable<Product> GetByUser(string UserName, string category)
        {
            return products.Where(p => p.UserName == UserName && p.Category == category);
        }

        public Product Add(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }
    }
}