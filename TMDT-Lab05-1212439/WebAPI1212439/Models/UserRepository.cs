﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212439.Models
{
    public class UserRepository : IUserRepository
    {
        private List<User> users = new List<User>();
      
        public UserRepository()
        { 
            Add(new User { UserName = "1212439", Password = "admin" });
            Add(new User { UserName = "leser", Password = "leser" });
            Add(new User { UserName = "tri", Password = "123456" });
        }

        public IEnumerable<User> GetAll()
        {
            return users;
        }

        public User Get(string UserName)
        {
            return users.Find(p => p.UserName == UserName);
        }

        public bool Add(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
         
            if(users.Exists(u => u.UserName == item.UserName))
            {
                return false;
            }
            users.Add(item);
            return true;
        }

        public bool Remove(string UserName)
        {
            var deleteUser = users.FirstOrDefault(u => u.UserName == UserName);
            if (deleteUser == null)
            {
                return false;
            }
            users.Remove(deleteUser);
            return true;
        }

        public bool Update(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = users.FindIndex(p => p.UserName == item.UserName);
            if (index == -1)
            {
                return false;
            }
            users.RemoveAt(index);
            users.Add(item);
            return true;
        }
    }
}

