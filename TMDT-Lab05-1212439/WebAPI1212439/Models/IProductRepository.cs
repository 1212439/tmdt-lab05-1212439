﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212439.Models
{
    interface IProductRepository
    {
        
            IEnumerable<Product> Get(string category);
            IEnumerable<Product> GetByUser(string category, string UserName);

            Product Add(Product item);
       

    }
}
